package ru.kev.manyfile;

import java.io.*;

/**
 * В данном классе классе реализованно формирование файла целых чисел.
 *
 */
public class ManyFile {
    public static void main(String[] args) throws IOException {
        BufferedReader read = new BufferedReader(new FileReader(new File("intdata.txt")));
        DataOutputStream output = new DataOutputStream((new FileOutputStream(new File("intdata.dat"))));
        String stringInt;
        while ((stringInt = read.readLine())!= null){

            output.writeInt(Integer.valueOf(stringInt));
        }

        read.close();
        output.close();

    }
}
