package ru.kev.manyfile;

import java.io.*;

/**
 * В данном классе реализован задача формирование файла с 6-ти значными числами.
 *
 * @author Karnauhov Evgeniy 15OIT18.
 */
public class ManyFile2 {
    public static void main(String[] args) throws IOException {
        DataInputStream input = new DataInputStream(new FileInputStream(new File("intdata.dat")));
        DataOutputStream output = new DataOutputStream(new FileOutputStream(new File("int6data.dat")));
        while (input.available()>0) {
            int i = input.readInt();
            if (i < 1000000 && i >= 0) {
                output.writeInt(i);
            }
        }
        input.close();
        output.close();
    }

}

