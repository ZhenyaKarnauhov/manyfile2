package ru.kev.manyfile;


import java.io.*;

/**
 * В данном классе реализованна задача с формированием файла целых чисел с "счастливыми" номерами.
 *
 * @author Karnauhov Evgeniy 15OIT18.
 */
public class ManyFile3 {
    public static void main(String[] args) throws IOException {
        DataInputStream input = new DataInputStream(new FileInputStream(new File("int6data.dat")));
        DataOutputStream output = new DataOutputStream(new FileOutputStream(new File("txt6data.dat")));
        while (input.available()>0){
            int i = input.readInt();
            if (sum(i/1000)==sum(i%1000)){
                output.writeInt(i);
            }
        }
        input.close();
        output.close();
    }

    /**
     * Метод считает сумму 3 чисел.
     *
     * @param i трехзначное число.
     * @return сумма трехзначного числа.
     */
    private static int sum(int i) {
        return i%10 + i/10%10 + i/100%10;
    }

}
